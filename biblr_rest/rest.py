from flask_restful import Resource, reqparse, abort

from .biblr_rest import db, api, app
from .models import Comment, User, Verse



def abort_if_missing(model, id):
    '''Aborts response if particular model id is not found in model table

    Args:
      model (SQLAlchemy model): SQLAlchemy model to check
      id (int): id (primary key) value to check if exists

    Examples:

    abort_if_missing(User, user_id)
    abort_if_missing(Verse, verse_id)
    abort_if_missing(Comment, comment_id)

    '''
    if not model.query.filter_by(id=id).first():
        abort(404, message="{} {} doesn't exist".format(model.__name__, id))


#----------------------------------------------------------------------
# Create RequestParser
#
# Create an instance of reqparse.RequestParser(), called "parser", and
# add to it the following arguments:
#
# - name
# - phone
# - email
# - password
# - text
# - verse_id (type=int)

parser = reqparse.RequestParser()
parser.add_argument('name')
parser.add_argument('phone')
parser.add_argument('email')
parser.add_argument('password')
parser.add_argument('text')
parser.add_argument('verse_id', type=int)



#----------------------------------------------------------------------
# REST API Endpoints
#----------------------------------------------------------------------
#
# Create REST API Resources that provide the following URL endpoints
#



#----------------------------------------------------------------------
# User Endpoints
#----------------------------------------------------------------------
# /users
#
# GET: return all User objects
#
# POST:
#
# - Use the parser (created above) to get the following attributes
#   from the request form
#   - name
#   - phone
#   - email
#   - password
#
# - Using those attributes, insert a single User into database,
#   return that User object with a code of 201
#

class UserListAPI(Resource):
    @User.marshal
    def get(self):
        return(User.query.all())

    @User.marshal
    def post(self):
        args = parser.parse_args()
        user = User(
            name=args['name'],
            phone=args['phone'],
            email=args['email'],
            password=args['password']
        )
        db.session.add(user)
        db.session.commit()
        return user, 201

api.add_resource(UserListAPI, '/users')
#----------------------------------------------------------------------
# /users/<int:user_id>
#
# GET: return first User object with id=user_id
#

class UserAPI(Resource):
    @User.marshal
    def get(self, user_id):
        abort_if_missing(User, user_id)
        return User.query.filter_by(id=user_id).first()

api.add_resource(UserAPI, '/users/<int:user_id>')


#----------------------------------------------------------------------
# /users/<int:user_id>/comments
#
# GET: return all Comment objects for User with id=user_id
#
# POST:
#
# - Use the parser (created above) to get the following attributes
#   from the request form
#   - text
#   - verse_id
#
# - Using those attributes as well as the user_id parameter, insert a
#   single Comment into the database
#

class UserCommentsAPI(Resource):
    @Comment.marshal
    def get(self, user_id):
        abort_if_missing(User, user_id)
        user = User.query.filter_by(id=user_id).first()
        return user.comments

    @Comment.marshal
    def post(self, user_id):
        abort_if_missing(User, user_id)
        user = User.query.filter_by(id=user_id).first()
        args = parser.parse_args()
        comment = Comment(text=args['text'], verse_id=args['verse_id'])
        user.comments.append(comment)
        db.session.commit()
        return comment, 201

api.add_resource(UserCommentsAPI, '/users/<int:user_id>/comments')


#----------------------------------------------------------------------
# Verse Endpoints
#----------------------------------------------------------------------
# /verses
#
# GET: return all Verse objects
#

class VerseListAPI(Resource):
    @Verse.marshal
    def get(self):
        return Verse.query.all()

api.add_resource(VerseListAPI, '/verses')


#----------------------------------------------------------------------
# /verses/<int:verse_id>
#
# GET: return first Verse object with id=verse_id

class VerseAPI(Resource):
    @Verse.marshal
    def get(self, verse_id):
            abort_if_missing(Verse, verse_id)
            verse = Verse.query.filter_by(id=verse_id).first()
            return verse

api.add_resource(VerseAPI, '/verses/<int:verse_id>')


#----------------------------------------------------------------------
# /verses/<book>
#
# GET: return all Verse objects with book=book
#

class VerseListBookAPI(Resource):
    @Verse.marshal
    def get(self, book):
        return Verse.query.filter_by(book=book).all()


api.add_resource(VerseListBookAPI, '/verses/<book>')


#----------------------------------------------------------------------
# /verses/<book>/<int:chapter>
#
# GET: return all Verse objects with book=book and chapter=chapter
#

class VerseListBookChapterAPI(Resource):
    @Verse.marshal
    def get(self, book, chapter):
        return Verse.query.filter_by(book=book, chapter=chapter).all()

api.add_resource(VerseListBookChapterAPI, '/verses/<book>/<int:chapter>')



#----------------------------------------------------------------------
# /verses/<book>/<int:chapter>/<int:verse>
#
# GET: return all Verse objects with book=book, chapter=chapter and
# verse=verse
#

class VerseBookChapterAPI(Resource):
    @Verse.marshal
    def get(self, book, chapter, verse):
        return Verse.query.filter_by(book=book, chapter=chapter, verse=verse).all()

api.add_resource(VerseBookChapterAPI, '/verses/<book>/<int:chapter>/<int:verse>')




#----------------------------------------------------------------------
# /verses/<int:verse_id>/comments
#
# GET: return all Comment objects related to Verse object with
# id=verse_id
#

class VerseCommentsAPI(Resource):
    @Comment.marshal
    def get(self, verse_id):
        abort_if_missing(Verse, verse_id)
        verse = Verse.query.filter_by(id=verse_id).first()
        return verse.comments

api.add_resource(VerseCommentsAPI, '/verses/<int:verse_id>/comments')


#----------------------------------------------------------------------
# Comment Endpoints
#----------------------------------------------------------------------
# /comments
#
# GET: return all Comment objects

class CommentListAPI(Resource):
    @Comment.marshal
    def get(self):
        return Comment.query.all()

api.add_resource(CommentListAPI, '/comments')



#----------------------------------------------------------------------
# /comments/<int:comment_id>
#
# GET: return first Comment object with id=comment_id

class CommentAPI(Resource):
    @Comment.marshal
    def get(self, comment_id):
        return Comment.query.filter_by(id=comment_id).first()

api.add_resource(CommentAPI, '/comments/<int:comment_id>')





#----------------------------------------------------------------------
# Search Endpoints
#----------------------------------------------------------------------
# /search/verses
#
# POST:
#
# - Use the parser (created above) to get the following attributes
#   from the request form
#   - text
#
# - Using that attribute, return all Verse objects whose "text" is
#   (case-insensitive) like it
#

class SearchVersesAPI(Resource):
    @Verse.marshal
    def post(self, search):
        args = parser.parse_args()
        verse = Verse(search=args['text'])
        return session.query(Verse).filter(Verse.text.ilike('%'+verse+'%')).all()

api.add_resource(SearchVersesAPI, '/search/verses')


#----------------------------------------------------------------------
# /search/users
#
# POST:
#
# - Use the parser (created above) to get the following attributes
#   from the request form
#   - name
#
# - Using that attribute, return the first User object with name=name
#

class SearchUserAPI(Resource):
    @User.marshal
    def post(self):
        args = parser.parse_args()
        name = User(name=args['name'])
        return User.query.filter_by(name=name).first()

api.add_resource(SearchUserAPI, '/search/users')

#----------------------------------------------------------------------
# /search/comments
#
# POST:
#
# - Use the parser (created above) to get the following attributes
#   from the request form
#   - text
#
# - Using that attribute, return all Comment objects whose "text" is
#   (case-insensitive) like it
#
