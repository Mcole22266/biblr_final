import os

from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.update(dict(
    SQLALCHEMY_DATABASE_URI='sqlite:///{}'.format(
        os.path.join(app.root_path, 'biblr.db')
    ),
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
))

db = SQLAlchemy(app)
api = Api(app)
