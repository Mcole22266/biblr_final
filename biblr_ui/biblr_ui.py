import os

from flask import Flask

app = Flask(__name__)
app.config.from_object(__name__)
app.config.update(dict(
    BIBLR_REST=os.environ.get('BIBLR_REST', 'http://localhost:5000'),
))
