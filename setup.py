from setuptools import setup

setup(
    name='biblr',
    packages=['biblr_rest', 'biblr_ui'],
    include_package_data=True,
    install_requires=[
        'flask',
        'requests',
        'sqlalchemy',
        'flask-sqlalchemy',
        'flask-restful',
        'pytest',
    ],
)
