import os
import json
import tempfile
import atexit

from biblr_rest import app, api, db
import biblr_rest.models as models
from biblr_rest.models import get_verses, get_users
import biblr_rest.rest as rest

temp_dir = tempfile.TemporaryDirectory()
print('Creating temp dir:', temp_dir.name)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(
    os.path.join(temp_dir.name, 'biblr.db')
)
app.testing = True
db.init_app(app)
api.init_app(app)
db.create_all()

client = app.test_client()

def remove_tempdir():
    print('Removing temp dir:', temp_dir.name)
    db.session.close()
    temp_dir.cleanup()
atexit.register(remove_tempdir)

def asdict(o, cols):
    return {c: getattr(o, c) for c in cols}

def populate_verses():
    '''This will populate the Verse table of your temp database
    with the first 10 verses if you wish to use it.
    '''
    verse_gen = get_verses()
    verses = [next(verse_gen) for i in range(10)]
    db.session.add_all(models.Verse(**v) for v in verses)
    db.session.commit()
    return [asdict(v, ['id','book','chapter','verse','text'])
            for v in models.Verse.query.all()]

def populate_users():
    '''This will populate the User table of your temp database
    with the first 10 users if you wish to use it.
    '''
    user_gen = get_users()
    users = [next(user_gen) for i in range(10)]
    db.session.add_all(models.User(**v) for v in users)
    db.session.commit()
    return [asdict(u, ['id','name','phone','email','password'])
            for u in models.User.query.all()]
